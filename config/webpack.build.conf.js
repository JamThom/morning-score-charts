/* eslint-disable */

const webpack = require('webpack')
const base = require('./webpack.base.conf')

var config = Object.assign({}, base)

config.output.libraryTarget = 'umd'
config.output.umdNamedDefine = true
config.output.filename = '[name].module.js'
config.plugins = (config.plugins || []).concat([
  new webpack.DefinePlugin({
    'process.env': {
      NODE_ENV: '"production"'
    }
  }),
])

module.exports = config
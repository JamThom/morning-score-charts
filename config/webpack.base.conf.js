/* eslint-disable */

const path = require('path');
const webpack = require('webpack');
const projectRoot = path.resolve(__dirname, '../');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
  mode: 'production',
  entry: {
    style: './src/scss/index.scss',
    main: './src/js/index.js'
  },
  output: {
    path: path.resolve(__dirname, '../dist'),
    filename: '[name].script.js',
    library: 'morningCharts',
    libraryTarget: 'var',
    umdNamedDefine: true
  },
  resolve: {
    extensions: ['.js'],
    alias: {
      '@': '../src/',
    }
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        loader: 'babel-loader',
        include: projectRoot,
        exclude: /node_modules/,
        options: {
          presets: [["env", {
            "targets": {
              "browsers": ["last 2 versions", "safari >= 7"]
            }
          }]]
        }
      }, {
        test: /\.(css|sass|scss)$/,
        use: ExtractTextPlugin.extract({
          use: ['css-loader', 'sass-loader'],
        })
      }
    ]
  },
  externals : {
    d3 : 'd3'
  },
  plugins: [
    new ExtractTextPlugin({
      filename: '[name].css',
      allChunks: true,
    }),
  ]
}
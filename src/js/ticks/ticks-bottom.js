import Ticks from './ticks'

class TicksBottom extends Ticks {

  constructor(props) {
    super(props)
    this._height = props.height
    this.element.attr('transform',`translate(0,${props.height-5})`)
    this.element.classed(`ticks ticks--bottom`,true)
    this.axis = this._generateAxis()
    this.element.call(this.axis)
  }

  get height() {
    return this._height
  }

  set height(v) {
    this._height = v
    console.log(v)
    this.element.attr('transform',`translate(0,${v-5})`)
  }

  _generateAxis() {
    return d3.axisBottom(this.scale)
    .tickSize(100,0)
  }

}

export default TicksBottom
class Ticks {

  constructor(props) {
    this.count = props.count
    this.minor = props.minor
    this.element = this._createGroup(props.target,props.orientation)
    this.scale = props.scale
  }

  _updateTransition() {
    this.axis = this._generateAxis()
    this.element.transition().duration(1500).call(this.axis)
  }

  _createGroup(target,orientation) {
    const g = target.append('g')
    g.classed('ticks--minor',this.minor)
    g.classed('ticks--major',!this.minor)
    return g
  }

  updateScale(scale) {
    this.scale = scale
    this._updateTransition()
  }

}

export default Ticks
import TicksLeft from './ticks-left'
import TicksBottom from './ticks-bottom'
import TicksBottomTime from './ticks-bottom-time'

export { TicksLeft, TicksBottom, TicksBottomTime }
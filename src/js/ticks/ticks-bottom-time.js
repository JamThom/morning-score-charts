import Ticks from './ticks-bottom'

class TicksBottomMonths extends Ticks {

  constructor(props) {
    super(props)
  }

  get res() {
    const dates = this.scale.domain()
    const timeDiff = Math.abs(dates[0] - dates[1])
    const diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24))
    return this._getResFromDays(diffDays)
  }

  get labels() {
    return {
      days: ['SØN','MAN','TIRS','ONS','TORS','FRE','LØR'],
      months: ['JAN','FEB','MAR','APR','MAJ','JUN','JUL','AUG','SEP','OKT','NOV','DEC'],
      hours: []
    }[this.res]
  }

  get ticksScale() {
    return {
      days: d3.timeDay.every(1),
      months: d3.timeMonth.every(1),
      hours: d3.timeHour.every(2)
    }[this.res]
  }

  _getResFromDays(days) {
    if (days < 30) {
      return this.minor ? 'hours' : 'days'
    } else {
      return this.minor ? 'days' : 'months'
    }
  }

  _getLabelFromDate(d) {
    switch (this.res) {
      case 'days':
        return this.labels[d.getDay()]
      break;
      case 'months':
        return this.labels[d.getMonth()]
      break;
      default:
        return d.getHours()
      break;
    }
  }

  _generateAxis() {
    return d3.axisBottom(this.scale)
    .ticks(this.ticksScale)
    .tickSize(this.minor?6:20,0)
    .tickFormat((d,i) => this._getLabelFromDate(d))
  }

}

export default TicksBottomMonths
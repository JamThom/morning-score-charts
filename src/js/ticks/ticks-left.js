import Ticks from './ticks'

class TicksLeft extends Ticks {

  constructor(props) {
    super(props)
    this.width = props.width
    this.element.classed(`ticks ticks--left`,true)
    this.axis = this._generateAxis()
    this.element.call(this.axis)
  }

  get width() {
    return this._width||100
  }

  set width(v) {
    this._width = v
    this.element.attr('transform',`translate(${this.width},0)`)
  }

  _generateAxis() {
    return d3.axisLeft(this.scale)
    .ticks(this.count)
    .tickSize(this.width,0)
    .tickFormat(d3.format("d"))
  }

}

export default TicksLeft
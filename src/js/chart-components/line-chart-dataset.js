import { DatasetPath, TooltipMarkers } from './'

class LineChartDataset {

  constructor(dataset,parent) {
    this.chartKey = parent.chartKey
    this.pathsGroup = parent.pathsGroup
    this.scales = parent.scales
    this.element = parent.element
    this.entries = dataset.entries
    this.path = this._createDatasetPath(dataset)
    this.key = this.chartKey.addDataset(dataset)
    this.markers = this._createTooltipMakers(dataset)
  }

  _createDatasetPath(dataset) {
    return new DatasetPath({
      target: this.pathsGroup,
      scales: this.scales,
      dataset
    })
  }

  _createTooltipMakers(dataset) {
    return new TooltipMarkers({
      dataset,
      target: this.element,
      scales: this.scales
    })
  }

  update(dataset) {
    if (dataset) this.entries = dataset.entries
    this.path.update(dataset)
    this.markers.update(dataset)
  }

  remove() {
    this.key.remove()
    this.path.remove()
    this.markers.remove()
  }

}

export default LineChartDataset
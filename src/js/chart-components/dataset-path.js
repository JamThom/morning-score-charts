class DatasetPath {

  constructor(props) {
    this.scales = props.scales
    this.data = this._calcInterpolation(props.dataset.entries)
    this.fill = props.dataset.color
    this.element = this._createGroup(props.target)
    this.path = this._createArea()
    this.line = this._createLine()
    this._animateUpdate(this.data)
  }

  _createGroup(target) {
    const g = target.append('g')
    g.classed('dataset-path',true)
    return g
  }

  _calcInterpolation(entries) {
    const steps = 300
    const step = entries.length / steps
    const points = [...new Array(steps)].map(
      (d,i) => {
        const s = Math.floor(step * i)
        if (entries[s+1] === undefined) {
          return entries[s]
        } else {
          return {
            value: d3.interpolate(entries[s].value,entries[s+1].value)(step*i%1),
            date: d3.interpolate(new Date(entries[s].date),new Date(entries[s+1].date))(step*i%1)
          }
        }
      }
    )
    return points
  }

  _createArea() {
    return this.element.append('path')
      .data([this._flattenValues()])
      .attr('fill',this.fill)
      .attr('opacity',0)
      .classed('dataset-path__area',true)
      .attr('d',this._generateAreaFunc())
  }

  _createLine() {
    return this.element.append('path')
      .data([this._flattenValues()])
      .classed('dataset-path__line',true)
      .attr('stroke',this.fill)
      .attr('opacity',0)
      .attr('d',this._generateLineFunc())
  }

  _generateLineFunc() {
    return d3.line()
      .x(d=>this.scales.x(new Date(d.date)))
      .y(d=>this.scales.y(d.value))
  }

  _generateAreaFunc() {
    return d3.area()
      .x(d=>this.scales.x(new Date(d.date)))
      .y1(d=>this.scales.y(d.value))
      .y0(this.scales.y.range()[0])
  }

  _flattenValues() {
    const r = this.data.slice(0)
    return r.map(d=> ({
      value: 0,
      date: d.date
    }))
  }

  _animateUpdate(data) {
    this.line
      .data([data])
      .transition()
      .duration(1500)
      .attr('d',this._generateLineFunc())
      .attr('opacity',1)
    this.path
      .data([data])
      .transition()
      .duration(1500)
      .attr('d',this._generateAreaFunc())
      .attr('opacity',1)
  }

  remove() {
    this._animateUpdate(this._flattenValues())
    this.element
      .attr('opacity',1)
      .transition()
      .duration(1500)
      .attr('opacity',0)
    setTimeout(x=>{
      this.element.remove()
    }, 1500)
  }

  update(dataset) {
    if (dataset) this.data = this._calcInterpolation(dataset.entries);
    this._animateUpdate(this.data)
  }

}

export default DatasetPath
class TooltipMarkers {

  constructor(props) {
    this.scales = props.scales
    this.fill = props.dataset.color
    this.element = this._createGroup(props.target)
    this.scales = props.scales
    this._createDots(props.dataset.entries)
    this.update(props.dataset)
  }

  _createGroup(target) {
    const g = target.append('g')
    g.classed('tooltip-markers',true)
    return g
  }

  _createDots(data) {
    this.element.selectAll('circle')
      .data(data)
      .enter()
      .append('circle')
  }

  _enterDots() {
    this.element.selectAll('circle')
      .data(this.data)
      .enter()
      .append('circle')
      .attr('fill',this.fill)
      .attr('r',60)
  }

  _exitDots() {
    this.element.selectAll('circle')
      .data(this.data)
      .exit()
      .remove()
  }

  _updateDots() {
    return this.element.selectAll('circle')
      .data(this.data)
      .classed('tooltip-markers__dot',true)
      .attr('cx',d=>this.scales.x(new Date(d.date)))
      .attr('cy',d=>this.scales.y(d.value))
      .attr('fill',this.fill)
      .attr('r',6)
  }

  _getClosestData(x,y) {
    let closestDist = Infinity
    let closestData
    this.dots.each((d,i,e)=> {
      const dist = Math.abs(e[i].getBoundingClientRect().left - x)
      if (dist < closestDist) {
        closestDist = dist
        closestData = d
      }
    })
    .classed('active',d=>d===closestData)
    return closestData
  }

  deactivateDots() {
    this.dots.classed('active',false)
  }

  remove() {
    this.dots.remove()
  }

  update(dataset) {
    if (dataset) this.data = dataset.entries
    this._exitDots()
    this._createDots(this.data)
    this.dots = this._updateDots()
  }

}

export default TooltipMarkers
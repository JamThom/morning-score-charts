class ChartKey {

  constructor(props) {
    this.selector = props.selector
    this.element = this._createElement()
    this.dataset = {}
  }

  _createElement() {
    return d3.select(this.selector).append('div')
      .classed('chart-key',true)
  }

  _createDatasetElement(dataset) {
    const ele = this.element.append('div')
    ele.classed('chart-key__item',true)
    this._createDotElement(ele,dataset.color)
    this._createTextElement(ele,dataset.name)
    return ele
  }

  _createTextElement(target,text) {
    return target.append('span')
      .text(text)
      .classed('chart-key__text',true)
  }

  _createDotElement(target,color) {
    return target.append('i')
      .classed('chart-key__dot',true)
      .style('background-color',color)
  }

  addDataset(dataset) {
    return this.dataset[dataset.slug] = {
      element: this._createDatasetElement(dataset),
      remove: x => this.removeDataset(dataset.slug)
    }
  }

  removeDataset(slug) {
    const ele = this.dataset[slug].element.node()
    ele.style.width = `${ele.offsetWidth}px`
    setTimeout(x=>{
      ele.style.width = 0
      ele.style.opacity = 0
    }, 10)
    setTimeout(x=>{
      this.dataset[slug].element.remove()
      delete this.dataset[slug]
    },1500)
  }

}


export default ChartKey
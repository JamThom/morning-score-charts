class Tooltip {

  constructor(props) {
    this.wrapper = this._createElement(d3.select(props.selector),'tooltip__wrapper')
    this.element = this._createElement(this.wrapper,'tooltip')
    this.textUpper = this._createElement(this.element,'tooltip__text tooltip__text--upper')
    this.textLower = this._createElement(this.element,'tooltip__text tooltip__text--lower')
  }

  _createElement(target,className) {
    return target.append('div')
      .classed(className,true)
  }

  update(data) {
    this.textUpper.text(data.textUpper)
    this.textLower.text(data.textLower)
    this.element.style('top', `${data.y*100}%`)
    this.element.style('left', `${data.x*100}%`)
  }

  show() {
    this.element.style('display', 'block')
  }

  hide() {
    this.element.style('display','none')
  }

}

export default Tooltip
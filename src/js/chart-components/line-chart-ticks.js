import { TicksLeft, TicksBottomTime } from '../ticks'

class LineChartTicks {

  constructor(props) {
    this.scales = props.scales
    this._width = props.width
    this._height = props.height
    this.ticksGroup = props.target.append('g')
    this.ticksLeft = this._createTicksLeft()
    this.ticksBottomMinor = this._createTicksBottomMinor()
    this.ticksBottomMajor = this._createTicksBottomMajor()
  }

  get height() {
    return this._height
  }

  set height(v) {
    this.ticksBottomMajor.height = v
    this.ticksBottomMinor.height = v
    this._height = v
  }

  get width() {
    return this._width
  }

  set width(v) {
    this.ticksLeft.width = v
    this._width = v
  }

  _createTicksLeft() {
    return new TicksLeft({
      target: this.ticksGroup,
      scale: this.scales.y,
      width: this.width,
      count: 3
    })
  }

  _createTicksBottomMinor() {
    return new TicksBottomTime({
      target: this.ticksGroup,
      minor: true,
      scale: this.scales.x,
      height: this.height
    })
  }

  _createTicksBottomMajor() {
    return new TicksBottomTime({
      target: this.ticksGroup,
      scale: this.scales.x,
      height: this.height
    })
  }

  update() {
    this.ticksLeft.updateScale(this.scales.y)
    this.ticksBottomMajor.updateScale(this.scales.x)
    this.ticksBottomMinor.updateScale(this.scales.x)
  }

}

export default LineChartTicks
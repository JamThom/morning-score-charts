class Chart {

  constructor(props) {
    this.maxValue = 0
    this.selector = props.selector
    this.element = this._createSvg()
    this._width = props.width
    this._height = props.height
    this._updateDimensions()
    this.datasets = {}
  }

  get width() {
    return this._width
  }

  set width(v) {
    this._width = v
    this._updateDimensions()
  }

  set height(v) {
    this._height = v
    this._updateDimensions()
  }

  get height() {
    return this._height
  }

  _createSvg() {
    return d3.select(this.selector).append('svg')
      .classed('chart',true)
  }

  addDataset(dataset) {
    this._checkUpdate(dataset)
  }

  removeDataset(key) {
    return null
  }

}

export default Chart
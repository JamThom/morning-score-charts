import Chart from './chart'
import { ChartKey, Tooltip, LineChartDataset, LineChartTicks } from '../chart-components'

class LineChart extends Chart {

  constructor(props) {
    super(props)
    d3.select(props.selector).classed('line-chart',true)
    this.element.on('mousemove',e=>this._mousemove(e))
    this.element.on('mouseout',e=>this.tooltip.hide())
    this.chartKey = this._createChartKey(data)
    this.tooltip = new Tooltip({ selector: this.selector })
    this.scales = this._createScales()
    this.ticks = this._createTicks()
    this.pathsGroup = this.element.append('g')
    props.data&&props.data.forEach(d=>this.addDataset(d))
    this.baseline = this._createBaseline()
  }

  get highlighted() {
    return this._highlighted
  }

  set highlighted(v) {
    this.datasets[this.highlighted] && this.datasets[this.highlighted].markers.deactivateDots()
    return this._highlighted = v
  }

  _generateScaleX() {
    return d3.scaleTime()
      .domain([new Date(this.startValue),new Date(this.endValue)])
      .range([0,this.width])
  }

  _generateScaleY() {
    return d3.scaleLinear()
      .domain([0,this.maxValue])
      .range([this.height,0])
  }

  _createTicks() {
    return new LineChartTicks({
      scales: this.scales,
      width: this.width,
      height: this.height,
      target: this.element
    })
  }

  _createBaseline() {
    return (this.baseline || this.element.append('line'))
      .attr('x1',0)
      .attr('x2',this.width)
      .attr('y1',this.height)
      .attr('y2',this.height)
      .classed('line-chart__baseline',true)
  }

  _createChartKey(data) {
    return new ChartKey({
      selector: this.selector
    })
  }

  _getMaxRoundedValue(entries) {
    let maxVal = d3.max(entries,d=>d.value)
    let maxRounded = 0;
    let i = 0;
    while (maxRounded < maxVal) {
      i++
      maxRounded = i%3 ? Math.pow(10,i/3) : maxRounded * 2
    }
    return maxRounded
  }

  _getStartValue(entries) {
    return d3.min(entries,d=>new Date(d.date))
  }

  _getEndValue(entries) {
    return d3.max(entries,d=>new Date(d.date))
  }

  _checkUpdate(dataset) {
    this._checkUpdateScaling(dataset.entries)
    if (this.datasets[dataset.slug] !== undefined) {
      this.datasets[dataset.slug].update(dataset)
    } else {
      this.highlighted = dataset.slug
      this.datasets[dataset.slug] = new LineChartDataset(dataset,this)
    }
  }

  _updateTooltip(d) {
    this.tooltip.show()
    this.tooltip.update({
      textUpper: Math.floor(d.value),
      textLower: d.date,
      x: this.scales.x(new Date(d.date)) / this.width,
      y: this.scales.y(d.value) / this.height
    })
  }

  _updateDimensions() {
    this.element.attr('viewBox',`0 0 ${this.width} ${this.height}`)
    if (!this.scales) return
    this._refreshValues()
    this.scales.update()
    this.ticks.width = this.width
    this.ticks.height = this.height
    this.ticks.update()
    this._createBaseline()
    for (let d in this.datasets) { this.datasets[d].update() }
  }

  _createScales() {
    const self = this
    return {
      x: this._generateScaleX(),
      y: this._generateScaleY(),
      update() {
        this.x = self._generateScaleX()
        this.y = self._generateScaleY()
      }
    }
  }

  _mousemove() {
    if (!this.highlighted) return
    const e = d3.event
    this._updateTooltip(this.datasets[this.highlighted].markers._getClosestData(e.clientX))
  }

  _changeScaleValues(entries) {
    this.maxValue = this._getMaxRoundedValue(entries)
    this.startValue = this._getStartValue(entries)
    this.endValue = this._getEndValue(entries)
  }

  _refreshValues(entries) {
    this.maxValue = this.startValue = this.endValue = 0
    for (let d in this.datasets) { this._changeScaleValues(this.datasets[d].entries) }
  }

  _checkUpdateScaling(entries) {
    const maxValue = this.maxValue, startValue = this.startValue, endValue = this.endValue
    this._refreshValues()
    this._changeScaleValues(entries)
    if ((maxValue !== this.maxValue) || (startValue !== this.startValue) || (endValue !== this.endValue)) {
      this.scales.update()
      this.ticks.update()
    }
  }

  removeDataset(slug) {
    this.datasets[slug].remove()
    delete this.datasets[slug]
    this.highlighted = this.datasets[this.highlighted] ? this.highlighted : Object.keys(this.datasets)[0]
  }

}


export default LineChart
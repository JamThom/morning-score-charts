
# Morning Score Charts
> Collection of reusable classes for creating charts and infographics

## Install
### webpack:
```js
// js
import morningCharts from './dist/main.module.js'
```
```scss
// scss
@import './dist/style';
```
### basic:
```html
<link rel="stylesheet" href="./dist/style.css">
<script src="./main.script.js"></script>
```

## Usage
### Line chart:
```js
// js
var exampleChart = new morningCharts.LineChart({
  data: data, // parsed json data array, optional
  selector: '#example', // element for the chart is to be appended to
  width: 840, // viewbox width (in px)
  height: 260 // viewbox height (in px)
})

// to add or update datasets:

exampleChart.addDataset({
  "name": randomString(8),
  "slug": 'unique-data-id',
  "color": '#333', 
  "entries": [{ value: 1000, date: '1/1/1970' }]
})

// to add or remove datasets:

exampleChart.remove('unique-data-id')

// to resize the chart:

exampleChart.width = 400
exampleChart.height = 200
```

## Development
```bash
npm run dev
```
open `./examples/index.html` to debug

## Build
```bash
npm run build
```

## Classes
### Chart
The base chart class from which all other charts can be extended
#### Props
```js
{
  selector: String, // query selector for target dom node
  data: Array, // array of data (optional)
  width: Number, // Number of representing width in pixels (optional)
  height: Number, // Number of representing height in pixels (optional)
}
```

### LineChart extends Chart
LineChart presumes that the x-axis will be time-based and y-axis will be value-based, and that there will always be only three x-axis indicators. This will probably be refactored at some point since I wasn't sure how the other charts may vary
#### Props
```js
{
  ...base
}
```
#### Methods
```js
set width(400) // sets the width of the view box ratio
set height(400) // sets the height of the view box ratio
set highlighted('unique-dataset-id') // slug determining which dataset is used for the tooltip
addDataset(dataSet) // adds dataset to the chart. If dataset already exists chart updates entries
removeDataset('unique-dataset-id') // remove dataset from chart based on slug
```
### Ticks
The base chart class from which all other ticks can be extended. In order to get the shorter in-between lines on the x-axis, we need to add two sets of ticks - one minor and one major.
#### Props
```js
{
  target: Object, // d3 selected dom node
  scale: Function, // d3 scale function
  count: Number, // number of ticks (optional)
  minor: Boolean, // whether ticks should be major or minor (optional)
}
```

#### Methods
```js
updateScale() // trigger update animation for ticks scale
```
### TicksLeft extends Ticks
Ticks for the y-axis
#### Props
```js
{
  ...base,
  width: Number, // width of parent chart
}
```
#### Methods
```js
set width(chart.width) // sets the width of the view box ratio
set height(chart.height) // sets the height of the view box ratio
```
### TicksBottom extends Ticks
Ticks for the x-axis
#### Props
```js
{
  ...base
  height: Number, // height of parent chart
}
```
### TicksBottomTime extends TicksBottom
Ticks for the x-axis with the text formatted to read Danish month/weekday abbreviations
#### Props
```js
{
  ...base
  height: Number, // height of parent chart
}
```
### ChartKey
The key which is displayed in the top right corner of the chart
#### Props
```js
{
  selector: String, // query selector
}
```
#### Methods
```js
  addDataset(dataset) // add dataset to chart key
  removeDataset('unique-dataset-id') // remove dataset from chart key by slug
```
### DatasetPath
The filled-in lines which represent data displayed on the chart
#### Props
```js
{
  target: Object, // d3 selected dom node
  scales: Object, // reference to charts x and y scale functions
  dataset: Object, // dataset object
}
```
#### Methods
```js
  update(dataset) // updates line and area svg elements
  remove() // animates out, then removes svg elements
```
### LineChartDataset
This is how the LineChart class communicates with its data and its data's various svg elements
#### Props
```js
{
  dataset: Object, // the dataset
  parent: Object, // the parent class which the dataset is attached to
}
```
#### Methods
```js
  update(dataset) // updates entries and triggers update animation
  remove() // triggers removal of sub-classes
```
### TooltipMarkers
The dot displayed when the tooltip is visible
#### Props
```js
{
  target: Object, // d3 selected dom node
  scales: Object, // reference to charts x and y scale functions
  dataset: Object, // dataset object
}
```
#### Methods
```js
  update(dataset) // updates position based on entries
  remove() // removes all elements
```
### Tooltip
Generic tooltip, designed to be used in multiple contexts
#### Props
```js
{
  selector: String, // query selector for target dom node
}
```
#### Methods
```js
  update({
    textUpper: 'value', // lead text to be displayed on tooptip
    textLower: 'date', // sub text to be displayed on tooptip
    x: 0.5, // x position of tooltip from 0-1, example gets converted into 'left: 50%'
    y: 0.5, // y position of tooltip from 0-1, example gets converted into 'top: 50%'
  })
```